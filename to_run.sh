source /etc/environment
#for use mllib - netlib-java and jblas - Breeze
#sudo apt-get install libgfortran3 
#sudo apt-get install libatlas3-base libopenblas-base
#sudo update-alternatives --config libblas.so.3
#sudo update-alternatives --config liblapack.so.3
# to install opencv http://www.sysads.co.uk/2014/05/install-opencv-2-4-9-ubuntu-14-04-13-10/
sbt clean compile package
#run program
$SPARK_HOME/bin/spark-submit --class "spark4mn.Spark" --jars ./lib/opencv-2.4.9-7.jar,./lib/json4s-jackson_2.10-3.2.10.jar,./lib/json4s-native_2.10-3.2.10.jar,./lib/scala-io-core_2.10-0.4.3.jar,./lib/scala-io-file_2.10-0.4.2.jar,./lib/scala-arm_2.10-1.3.jar --master local[4] ./target/scala-2.10/simple-project_2.10-1.0.jar /home/alessio/Scrivania/tesi/example-spark/ 
#with uber jar
$SPARK_HOME/bin/spark-submit --class "spark4mn.Support" --driver-memory 2G --master local[4] ./target/scala-2.10/simple-project_2.10-1.0.jar /home/alessio/Scrivania/tesi/example-spark/

