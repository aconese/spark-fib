name := "Simple Project"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

libraryDependencies += "org.apache.spark" %% "spark-mllib" % "1.2.0"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.2.0"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.2.0"

libraryDependencies += "org.json4s" % "json4s-core_2.10" % "3.2.10"

libraryDependencies += "org.json4s" % "json4s-ast_2.10" % "3.2.10"

libraryDependencies += "org.json4s" % "json4s-jackson_2.10" % "3.2.10"

libraryDependencies += "org.json4s" % "json4s-native_2.10" % "3.2.10"

libraryDependencies += "org.scala-lang" % "scalap" % "2.10.4"

libraryDependencies += "com.thoughtworks.paranamer" % "paranamer" % "2.7"

libraryDependencies += "com.googlecode.netlib-java" % "netlib-java" % "1.1"

libraryDependencies += "org.jblas" % "jblas" % "1.2.3"

libraryDependencies += "org.xerial.snappy" % "snappy-java" % "1.1.1.3"

libraryDependencies += "com.github.fommil.netlib" % "all" % "1.1.2" pomOnly()

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"   % "2.3.6",
  "com.typesafe.akka" %% "akka-slf4j"   % "2.3.6",
  "com.typesafe.akka" %% "akka-remote"  % "2.3.6",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.6" % "test"
)

libraryDependencies  ++= Seq(
            // other dependencies here
            "org.scalanlp" % "breeze_2.10" % "0.10",
            // native libraries are not included by default. add this if you want them (as of 0.7)
            // native libraries greatly improve performance, but increase jar sizes.
             "org.scalanlp" % "breeze-natives_2.10" % "0.10"
)

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

resolvers ++= Seq("Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/")

libraryDependencies += "org.bytedeco" % "javacv" % "0.9"

libraryDependencies += "nu.pattern" % "opencv" % "2.4.9-7"

libraryDependencies += "org.scalaj" % "scalaj-http_2.10" % "0.3.16"

libraryDependencies += "com.github.scala-incubator.io" % "scala-io-core_2.10" % "0.4.3"

libraryDependencies += "com.github.scala-incubator.io" % "scala-io-file_2.10" % "0.4.3"

libraryDependencies += "com.jsuereth" %% "scala-arm" % "1.3"


mergeStrategy in assembly <<= (mergeStrategy in assembly) ((old) => {
  case x if Assembly.isConfigFile(x) =>
    MergeStrategy.concat
  case PathList(ps @ _*) if Assembly.isReadme(ps.last) || Assembly.isLicenseFile(ps.last) =>
    MergeStrategy.rename
  case PathList("META-INF", xs @ _*) =>
    (xs map {_.toLowerCase}) match {
      case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) =>
        MergeStrategy.discard
      case ps @ (x :: xs) if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") =>
        MergeStrategy.discard
      case "plexus" :: xs =>
        MergeStrategy.discard
      case "services" :: xs =>
        MergeStrategy.filterDistinctLines
      case ("spring.schemas" :: Nil) | ("spring.handlers" :: Nil) =>
        MergeStrategy.filterDistinctLines
      case _ => MergeStrategy.first // Changed deduplicate to first
    }
  case PathList(_*) => MergeStrategy.first // added this line
})

assemblyJarName in assembly := "simple-project_2.10-1.0.jar"
