First of all, you must type "module load SPARK4MN". After that, you will have available
spark4mn, spark4mn_benchmark, and spark4mn_plot in you PATH.

Please, type spark4mn -h, spark4mn_benchmark -h, or spark4mn_plot -h and read the help messages.

