-----
1.4.0
-----
  
  BUGS:
    * Fixed bug not checking NCORES_PER_WORKER_STEP_OPERATOR properly (retroactive for affected versions since 1.2.0).
    * Fixed bug not breaking the loop when NCORES_PER_WORKER, or WORKER_MEM_SIZE ranks are not provided in spark4mn_benchmark (retroactive for affected versions since 1.2.0). Thanks to Alessio Conese for reporting it.
    * Fixed bug not getting the node's geometry in spark4mn_plot.
    * Fixed errors passing empty string Jars flag, or empty string application parameters which can cause errors. Thanks to Rubèn Tous for reporting it.
    * Fixed error with spaces in paths affecting the Jars flag and the entry point Jar.
    * Fixed error not handling properly paths in CLASSPATH with '*' wildcard at the end of the line.
    * Fixed error not handling properly metrics paths with spaces in spark4mn_plot (retroactive for affected versions since 1.3.0).
    * Fixed error that sometimes a worker metric was missing.
    * Fixed error message not being able to delete some temporal files.
    * Fixed error in spark4mn_plot: Plot files were not removed and, because of this, the plot data was appended to the existing plot file.
    * Fixed bug not counting and showing how many metrics files were in each subdirectory if the restart flag was not present.
    * Fixed bug: timeout in Spark communications if the cluster has a hang.
    * Fixed bug in spark4mn_benchmark not passing the *_PARAMETERS properly. Always using the first generated parameters for the first geometry.
    * Fixed bug: some network comunication were using the wrong interface.
    * Fixed bug: some files were still have been created in /tmp not in TMPDIR.
    * Fixed but repeating geometries if a value was repeated in WORKER_AFFINITY_MODES, or NETWORK_INTERFACE_MODES.
  
  FEATURES:
    * Added primary Hadoop support for Spark (but HDFS is still not enabled). Not configurable.
    * Added reciprocal to prologue: Epilogue. Now it possible to run as many epilogues as needed after finishing the domains' executions. New variables have been created to configure epilogues in the same way as prologues.
    * Now, the communication network if defined by NETWORK_INTERFACE configuration variable in spark4mn. It is possible to define a list in spark4mn_benchmark with NETWORK_INTERFACE_MODES, to be able to test different networks in the same benchmark. Value of the network will be inside the plot files, the same as with core affinities.
    * spark4mn_benchmark can be restarted. If there is not the same amout of metrics files for each geometry, a job is sent for these goemetries. If no geometries are present restart flag is ignored but if there is the same amount for each geometry it does not sent any job (useful for making loop with spark4mn_benchmark).
    * Plot files now have not only the mean value of the metric (in case there is more than one metrics file for each geometry) but also the standard deviation.
    * spark4mn_plot now have two new flags (modes). One flag (-g) allow gaps in the plot if there is some geometries without metrics files. The other flag (-H) force the command to abort if there is not the same amount metrics files for each geometry.
  
  IMPROVEMENTS:
    * Removed warning about being unable to load Hadoop native libraries.
    * Now, NWORKERS_STEP is not a compulsory parameter for spark4mn_benchmark.
    * Now, NWORKERS_PER_NODE is not a compulsory parameter for spark4mn. MIN_NCORES_PER_WORKER, and MAX_NCORES_PER_WORKER are now also optional for spark4mn_benchmark.
    * Now, you can define MIN_* and MAX_* variables and not define *_STEP corresponding variables. Steps will be 1 with '+', or 2 with '*' step operator.
    * Now, EXECUTOR_* variables are called DOMAIN_*.
    * Now, DATA_LOAD_* variables are called PROLOG_*.
    * Removed EXECUTOR_OUTPUT_PATH, DATA_LOAD_INPUT_PATH, and *_ADDITIONAL_PARAMETERS. Replaced by *_PARAMETERS. Now, you can choose the order of the parameters, and the paths are detected and checked automatically in spark4mn. The same applies to spark4mn_benchmark, including EXECUTOR_OUTPUT_BASEPATH.
    * Now, in spark4mn, all relative paths are relative to WORKING_DIR variable. In spark4mn_benchmark, all relative paths are relative to WORKING_BASEDIR.
    * In spark4mn_benchmark, in *_PARAMETERS, all the paths must be preceded by IN, or OUT. Used to detect output paths, and create the proper jobs' subpaths that will avoid job write-to-file collisions.
    * Faster creation of jobs in spark4mn_benchmark.
    * Jobs now have a better way to terminate if they reach the wallclock, or if they are killed with bkill.
    * Better module error checks and messages.
    * Affinity type now inside the plot file as another field.
    * PID directory for Spark processes now inside the TMPDIR specific folder.
    * Enable "Garbage First Garbage Collector" (G1GC) with the Java options.
    * Now using the latest Java version: 1.8.0 u31 x64.
    * Now the return codes are monitored, and if one fails the job is aborted.
    * Some code clean-up.


-----
1.3.0
-----
  
  BUGS:
    * Fixed an error code with a bad value.
    * The check that ensures that at least one executor is provided was missing.
  
  FEATURES:
    * Added new command: spark4mn_plot.
  
  IMPROVEMENTS:
    * Now, DATA_LOAD_INPUT_PATH_[[1-9]+] is not a compulsory parameter for a data loader, as it could be an independent data generator.
    * Some code clean-up.


-----
1.2.0
-----
  
  BUGS:
    * Fixed bug missing EXECUTOR_OUTPUT_PATH (retroactive for affected version 1.1.0).
    * WORKER_AFFINITY_MODES not treated correctly if the space between commas was missing (retroactive for affected version 1.1.0).
    * Fixed bug not generating job geometries correctly (retroactive for affected version 1.1.0).
    * Renamed configuration variable from NTHREADS_PER_WORKER to NCORES_PER_WORKER because it was bad-named.
    * It was undocumented that paths inside ADDITIONAL_CLASSPATH, or previously loaded CLASSPATH, had to be absolute. Now, paths can be relative.
  
  FEATURES:
    * Added support for disabling CPU affinity on workers via NONE, or just not setting this configuration.
    * Added silent mode for production executions. Turns off logging to disk.
    * Added step operators for NWORKERS_STEP NWORKERS_PER_NODE_STEP NCORES_PER_WORKER_STEP WORKER_MEM_SIZE_STEP. Possible values '+', and '*'. By default is '*'.
  
  IMPROVEMENTS:
    * Added --driver-cores NCORES --driver-memory NODE_MEM_SIZE --total-executor-cores NWORKERS*NWORKERS_PER_NODE --executor-memory WORKER_MEM_SIZE tuning flags and SPARK_LOCAL_DIRS="TMPDIR/SUBDIR" tuning variables to "spark-submit".
    * Temporal metric output now outside the log files, in tmpfs.
    * Temporal files now outside the local disk, in tmpfs.
    * Improved file names to avoid job collision.
    * Some code clean-up.
    * Added checks to test if paths in CLASSPATH are accessible and Jars in CLASSPATH are readable.


------------
1.1.0, 1.0.0
------------
  
  * The life was so hard in the past to have had free time, and create this CHANGELOG.
  * Anyway, looking at the latest changes, you will get an idea of the amount of work needed to come up with these versions.
