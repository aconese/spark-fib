if [ $# -ne 2 ] 
	then
	echo -e "insert program name\n Spark or Support and time\n./write_b.sh Spark 100"
	exit 1
fi
rm -f tmp.conf
program=$1
minute=$2
for n_nodes in 8
do
	echo $n_nodes	
	for n_worker in 4 
	do
		echo -e "\t$n_worker"

		### COMPULSORY PARAMETERS:
		 echo "JOB_NAME=\"$program""_fib_$n_nodes""_$n_worker\"" >> tmp.conf
		 echo "WORKING_DIR=\"/gpfs/projects/bsc31/bsc31220/test/\""  >> tmp.conf
		 echo "WALLCLOCK=$minute"  >> tmp.conf
		 echo "SPARK_NNODES=$n_nodes" >> tmp.conf 
		 echo "DOMAIN_JAR_1=\"/gpfs/projects/bsc31/bsc31220/test/simple-project_2.10-1.0.jar\"" >> tmp.conf 
		 echo "DOMAIN_ENTRY_POINT_1=\"spark4mn.$program\"" >> tmp.conf 
		 echo "SPARK_NWORKERS_PER_NODE=$n_worker" >> tmp.conf 
		 
	         echo " DOMAIN_PARAMETERS_1=\"/gpfs/projects/bsc31/bsc31220/test/\"" >> tmp.conf
		
		spark4mn tmp.conf
		rm -f tmp.conf
	done
done


