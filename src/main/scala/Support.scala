	import org.apache.spark.mllib.classification.NaiveBayes
	import org.apache.spark.mllib.linalg.distributed.RowMatrix
	import org.apache.spark.SparkConf
	import org.apache.spark.SparkContext
	import org.apache.spark.SparkContext._
	import org.apache.spark.mllib.classification.SVMWithSGD
	import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
	import org.apache.spark.mllib.regression.LabeledPoint
	import org.apache.spark.mllib.linalg.Vectors
	import org.apache.spark.mllib.util.MLUtils
	import org.apache.hadoop.io.Text
	import org.apache.hadoop.io.BytesWritable
	import org.apache.spark.rdd.RDD
	import scala.reflect.macros.Context
	import org.json4s._
	import org.json4s.native.JsonMethods._
	import org.opencv.core.Core
	import org.opencv.core.Mat
	import org.opencv.highgui._
	import org.opencv.utils.Converters
	import org.apache.commons.io.FilenameUtils
	import java.util.Arrays
	import scala.collection.JavaConverters._
	import scala.collection.mutable.HashMap
	import org.opencv.core.CvType
	import org.apache.spark.mllib.linalg.{Vector, Vectors}
	import org.apache.spark.Accumulable
	import org.apache.spark.AccumulableParam
	import org.apache.spark.AccumulatorParam
	import org.apache.spark.serializer.JavaSerializer
	import scala.collection.mutable.ListBuffer
	import org.apache.spark.mllib.regression.LinearRegressionWithSGD
	import org.apache.spark.mllib.feature.HashingTF
	import org.apache.spark.mllib.feature.IDF
	import org.apache.spark.mllib.feature.Normalizer
	import org.apache.spark.mllib.optimization.L1Updater
	
object Support {
 	
    def main(args: Array[String]) {
    
      def getUsername(inputJson : String) : String = 
 		{implicit val formats = DefaultFormats 
	    	try{
	    		val conver = parse(inputJson) 
	    		val username =(conver \ "user" \ "username").extract[String]
	    		return username	
	    	}catch{
	    	  	 		  case e: Exception => println("error json")
	    	}
	    	return ""
	    	}
 	
 	def byteswritableToOpenCVMat(inputBW: BytesWritable): Mat =
 	  {			val imageFileBytes = inputBW.getBytes
				var img = new Mat(1,imageFileBytes.length,CvType.CV_8UC1)
 	  			
 	  			img.put(0, 0, imageFileBytes)
 	  			img = Highgui.imdecode(img, Highgui.CV_LOAD_IMAGE_COLOR)
 	  		
			return img
 		 }
 	
 	def byteswritableToString(input : BytesWritable) : String =
 		{
 		  val conver = input.getBytes()
 		  val converted : String = new String(conver,0,conver.length,java.nio.charset.Charset.forName("Cp1252"))
 		  return converted
		
 		}
 	
 	
 	def extractInfo(input : String) : List[String] =
		{	implicit val formats = DefaultFormats
 		    
 		  	try{
				val s:String=input.stripLineEnd
 		  	    val conver = parse(s)
	 		  	val obj = conver \ "tags"
	 		  	
				val values : List[String]  = obj.extract[List[String]]
		        return values.map(x=>x.toLowerCase())
 		  	}
			catch{
			  case e: Exception => println(e+"\nerror json:\n" + input)
			  }
 			 
 			 return null
	    	}
 	   
 		
 		/*return a mllib Vector. In each position there is 1.0 if the tag of the values list is present
 		 * in table of valid tags, 0.0 otherwise. The position of the value is determinated by the value in the 
 		 * table tags_table (key->index_of_the_key)
 		 * */
 	def calculateNaiveBayes(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) : (Double, Double, Double) =
	 		{
 			val splits = parsedData.randomSplit(Array(0.6, 0.4), seed = 11L)
			val training = splits(0)
			val test = splits(1)
			val model = NaiveBayes.train(training, lambda = 1.0)
			
			
			
				/*********START Quality evaluation*******/
			// Compute raw scores on the test set. 
			val scoreAndLabels = test.map { point =>
			val score = model.predict(point.features)
			(score, point.label)
			}
		
			// Get evaluation metrics.
			val metrics = new BinaryClassificationMetrics(scoreAndLabels)
			val auROC = metrics.areaUnderROC()

			val valuesAndPreds = test.map { point =>
			  val prediction = model.predict(point.features)
			  (point.label, prediction)
			}
			val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			
			
			val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
			val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
			
	    	(auROC,MSE,accuracy)
 		}
 	
 	def calculateLinearRegression(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) : (Double, Double, Double, Double, Double, Double) =
	 		{
 			// Building the model
			val numIterations = 50
			val model = LinearRegressionWithSGD.train(parsedData, numIterations)
			
			val LRalg = new LinearRegressionWithSGD()
			LRalg.optimizer.
			  setNumIterations(200).
			  setRegParam(0.1).
			  setUpdater(new L1Updater)
			val modelL1 = LRalg.run(parsedData)
			
				/*********START Quality evaluation*******/
			val test=parsedData
			// Compute raw scores on the test set. 
			val scoreAndLabels = test.map { point =>
			val score = model.predict(point.features)
			(score, point.label)
			}
		
			// Get evaluation metrics.
			val metrics = new BinaryClassificationMetrics(scoreAndLabels)
			val auROC = metrics.areaUnderROC()
			
			val scoreAndLabelsL1 = test.map { point =>
			val score = modelL1.predict(point.features)
			(score, point.label)
			}
		
			
			val metricsL1 = new BinaryClassificationMetrics(scoreAndLabelsL1)
			val auROCL1 = metricsL1.areaUnderROC()
			
			
			val valuesAndPreds = test.map { point =>
			  val prediction = model.predict(point.features)
			  (point.label, prediction)
			}
			val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			
			val valuesAndPredsL1 = test.map { point =>
			  val prediction = modelL1.predict(point.features)
			  (point.label, prediction)
			}
			val MSEL1 = valuesAndPredsL1.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
			val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
			val predictionAndLabelL1 = test.map(p => (modelL1.predict(p.features), p.label))
			val accuracyL1 = 1.0 * predictionAndLabelL1.filter(x => x._1 == x._2).count() / test.count()
			
	    	(auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1)
 		}
 	
 		
		def calculateSVM(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) :  (Double, Double, Double, Double, Double, Double) =
		{	//Load training data in LIBSVM format
			//val data = MLUtils.loadLibSVMFile(sc, "/home/alessio/Scrivania/tesi/sample_libsvm_data.txt")
			val data=parsedData			
			// Split data into training (60%) and test (40%).
			val splits = data.randomSplit(Array(0.6, 0.4), seed = 11L)
			
			val training = splits(0).cache()
			val test = splits(1)
		
			// Run training algorithm to build the model
			val numIterations = 100
			val model = SVMWithSGD.train(training, numIterations)
			
			val svmAlg = new SVMWithSGD()
			svmAlg.optimizer.
			  setNumIterations(200).
			  setRegParam(0.1).
			  setUpdater(new L1Updater)
			val modelL1 = svmAlg.run(training)
			
			// Clear the default threshold.
			model.clearThreshold()
			modelL1.clearThreshold()
			
			
			/*********START Quality evaluation*******/
			
			// Compute raw scores on the test set. 
			val scoreAndLabels = test.map { point =>
			val score = model.predict(point.features)
			(score, point.label)
			}
		
			// Get evaluation metrics.
			val metrics = new BinaryClassificationMetrics(scoreAndLabels)
			val auROC = metrics.areaUnderROC()
			
			
			val scoreAndLabelsL1 = test.map { point =>
			val score = modelL1.predict(point.features)
			(score, point.label)
			}
		
			
			val metricsL1 = new BinaryClassificationMetrics(scoreAndLabelsL1)
			val auROCL1 = metricsL1.areaUnderROC()
			
			
			val valuesAndPreds = test.map { point =>
			  val prediction = model.predict(point.features)
			  (point.label, prediction)
			}
			val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			
			val valuesAndPredsL1 = test.map { point =>
			  val prediction = modelL1.predict(point.features)
			  (point.label, prediction)
			}
			val MSEL1 = valuesAndPredsL1.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
			val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
			val predictionAndLabelL1 = test.map(p => (modelL1.predict(p.features), p.label))
			val accuracyL1 = 1.0 * predictionAndLabelL1.filter(x => x._1 == x._2).count() / test.count()
			
	    	(auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1)
			
 		  
	    }
 		
	
 		
 		def featurize(s : String) : Vector =
 		{				 
	    	val n = 1000
	    	val result = new Array[Double](n)
	    	val bigrams= s.sliding(2).toArray
	    	for (h <- bigrams.map(_.hashCode() %n))
	    		{
	    			result(h) += 1.0 /bigrams.length
	    		}
	    	Vectors.sparse(n, result.zipWithIndex.filter(_._1 != 0).map(_.swap))
 		    
 		    
 		 }
 	def obtainTDIDF (data: RDD[List[String]]) : RDD[Vector]=  
				      { 
					    val hashingTF = new HashingTF()
				      	val tf: RDD[Vector] = hashingTF.transform(data)
				      	tf.cache()
						val idf = new IDF().fit(tf)
						val tfidf: RDD[Vector] = idf.transform(tf)
						tfidf
				      }	
 	def obtainFeaturize (data: RDD[List[String]]) : RDD[Vector]=  
					{
				      val tags2=data.map(x=>x.mkString(" "))
					    tags2.map(featurize)
					    }
 	
	//nu.pattern.OpenCV.loadShared()
 	val conf = new SparkConf().setMaster("local[4]").setAppName("FIB-Instagram")
	val sc = new SparkContext(conf)
	val sqlContext = new org.apache.spark.sql.SQLContext(sc)


	//for read from sequence file
	val rdd_im  = sc.sequenceFile[Text,BytesWritable]("data/wc2014/wc14_1.hsf")
	//val rdd_json  = sc.sequenceFile[Text,BytesWritable]("data/wc2014/wc14_1_meta.hsf")
	//val rdd_json  = sc.textFile("data/wc2014/worldcup2014_1/*.txt")
	//val rdd_json  = sc.textFile("data/desigual-users-extended.txt")
	val rdd_json  = sc.textFile("data/mydata.txt")
	val rdd_json_neg  = sc.textFile("data/mydata2.txt")
	//process the data in the RDD
	val process_im=rdd_im.map(x => (FilenameUtils.removeExtension(FilenameUtils.getBaseName(x._1.toString())),byteswritableToOpenCVMat(x._2).cols()+"")) //to do  with openCV
	//val process_json=rdd_json.map(x => (FilenameUtils.removeExtension(FilenameUtils.getBaseName(x._1.toString)),extractInfo(byteswritableToString(x._2))))
	//val process_json=rdd_json.map(x => extractInfo(x))
	val process_json=rdd_json.filter(x => extractInfo(x) != null).map(x => extractInfo(x))
	val process_json_neg=rdd_json_neg.filter(x => extractInfo(x) != null).map(x => extractInfo(x))
	
      
     val data_tmpFeaturize : RDD[Vector]= obtainFeaturize(process_json)
	val data_tmpTDIDF : RDD[Vector]= obtainTDIDF(process_json)
	val data_tmpFeaturize_neg : RDD[Vector]= obtainFeaturize(process_json_neg)
	val data_tmpTDIDF_neg : RDD[Vector]= obtainTDIDF(process_json_neg)
 					
	val parsedData_Fea = data_tmpFeaturize.map { line => LabeledPoint(1.0,line) }
	val parsedData_FeaNeg = data_tmpFeaturize_neg.map { line => LabeledPoint(0.0,line) }
	val parsedData_TDIDF = data_tmpTDIDF.map { line => LabeledPoint(1.0,line) }
	val parsedData_TDIDFNeg = data_tmpTDIDF_neg.map { line => LabeledPoint(0.0,line) }
	println("END creation table\nbegin computation")
	
	val finalDataFea=parsedData_Fea.union(parsedData_FeaNeg)
	val finalDataTDIDF=parsedData_TDIDF.union(parsedData_TDIDFNeg)
	
	var results : Array[Any] = new Array[Any](3)
	results(1)=calculateSVM(sc,finalDataFea)
	results(2)=calculateSVM(sc,finalDataTDIDF)
	results(0)=("ROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t")
	
	var results1 : Array[Any] = new Array[Any](3)
	results1(1)=calculateNaiveBayes(sc,finalDataFea)
	results1(2)=calculateNaiveBayes(sc,finalDataTDIDF)
	results1(0)=("ROC\t-\tMSE\t-\taccurancy\t")
	
	var results2 : Array[Any] = new Array[Any](3)
	results2(1)=calculateLinearRegression(sc,finalDataFea)
	results2(2)=calculateLinearRegression(sc,finalDataTDIDF)
	results2(0)=("ROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t")
	
	println("\n\n\n\nResults of SVM:\n"+results(0))
	println(results(1) + " result of featurize" )
	println(results(2)+" result of TDIDF")

	
	
	println("\n\n\n\nResults of Naive Bayes:\n"+results1(0))
	println(results1(1) + " result of featurize" )
	println(results1(2)+" result of TDIDF")
	
	
	println("\n\n\n\nResults of Linear Regression:\n"+results2(0))
	println(results2(1) + " result of featurize" )
	println(results2(2)+" result of TDIDF")
	 
	

  }
}