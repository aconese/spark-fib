	import org.apache.spark.mllib.classification.NaiveBayes
	import org.apache.spark.mllib.linalg.distributed.RowMatrix
	import org.apache.spark.SparkConf
	import org.apache.spark.SparkContext
	import org.apache.spark.SparkContext._
	import org.apache.spark.mllib.classification.SVMWithSGD
	import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
	import org.apache.spark.mllib.regression.LabeledPoint
	import org.apache.spark.mllib.linalg.Vectors
	import org.apache.spark.mllib.util.MLUtils
	import org.apache.hadoop.io.Text
	import org.apache.hadoop.io.BytesWritable
	import org.apache.spark.rdd.RDD
	import scala.reflect.macros.Context
	import org.json4s._
	import org.json4s.native.JsonMethods._
	import org.opencv.core.Core
	import org.opencv.core.Mat
	import org.opencv.highgui._
	import org.opencv.utils.Converters
	import org.apache.commons.io.FilenameUtils
	import java.util.Arrays
	import scala.collection.JavaConverters._
	import scala.collection.mutable.HashMap
	import org.opencv.core.CvType
	import org.apache.spark.mllib.linalg.{Vector, Vectors}
	import org.apache.spark.Accumulable
	import org.apache.spark.AccumulableParam
	import org.apache.spark.AccumulatorParam
	import org.apache.spark.serializer.JavaSerializer
	import scala.collection.mutable.ListBuffer
	import org.apache.spark.mllib.regression.LinearRegressionWithSGD
	import org.apache.spark.mllib.feature.HashingTF
	import org.apache.spark.mllib.feature.IDF
	import org.apache.spark.mllib.optimization.L1Updater
	import scalax.io._
	import java.io.File
	
	
object Spark {
 	
    def main(args: Array[String]) {
       
 	def getUsername(inputJson : String) : String = 
 		{implicit val formats = DefaultFormats 
	    	try{
	    		val conver = parse(inputJson) 
	    		val username =(conver \ "user" \ "username").extract[String]
	    		return username	
	    	}catch{
	    	  	 		  case e: Exception => println("error json")
	    	}
	    	return ""
	    	}
 	
 	def byteswritableToOpenCVMat(inputBW: BytesWritable): Mat =
 	  {			val imageFileBytes = inputBW.getBytes
				var img = new Mat(1,imageFileBytes.length,CvType.CV_8UC1)
 	  			
 	  			img.put(0, 0, imageFileBytes)
 	  			img = Highgui.imdecode(img, Highgui.CV_LOAD_IMAGE_COLOR)
 	  		
			return img
 		 }
 	
 	def byteswritableToString(input : BytesWritable) : String =
 		{
 		  val conver = input.getBytes()
 		  val converted : String = new String(conver,0,conver.length,java.nio.charset.Charset.forName("Cp1252"))
 		  return converted
		
 		}
 	
 	
 	def extractInfo(input : String) : List[String] =
		{	implicit val formats = DefaultFormats
 		    
 		  	try{
				val s:String=input.stripLineEnd
 		  	    val conver = parse(s)
	 		  	val obj = conver \ "tags"
	 		  	
				val values : List[String]  = obj.extract[List[String]]
		        return values.map(x=>x.toLowerCase())
 		  	}
			catch{
			  case e: Exception => println(e+"\nerror json:\n" + input)
			  }
 			 
 			 return null
	    	}
 	   
 		
 		/*return a mllib Vector. In each position there is 1.0 if the tag of the values list is present
 		 * in table of valid tags, 0.0 otherwise. The position of the value is determinated by the value in the 
 		 * table tags_table (key->index_of_the_key)
 		 * */
 	 	def calculateNaiveBayes(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) : (Double, Double, Double) =
	 		{
 	 	  
 	 	  	val file: Seekable =  Resource.fromFile(new File("output.txt"))
			var result:( Double, Double, Double)= (0.0,0.0,0.0) 
			file.append("Naive Bayes results\nROC\t-\tMSE\t-\taccurancy\t-\ttime\n")
			
 			val splits = parsedData.randomSplit(Array(0.6, 0.4), seed = 11L)
			val training = splits(0)
			val test = splits(1)
			val lambdas= List(0.5,1.0,1.5)
			for (l<-lambdas){
				
				val s = System.currentTimeMillis
				val model = NaiveBayes.train(training, lambda = l)
				val time= System.currentTimeMillis - s
				
				/*********START Quality evaluation*******/
				
				// Compute raw scores on the test set. 
				val scoreAndLabels = test.map { point =>
				val score = model.predict(point.features)
				(score, point.label)
				}
			
				// Get evaluation metrics.
				val metrics = new BinaryClassificationMetrics(scoreAndLabels)
				val auROC = metrics.areaUnderROC()
	
				val valuesAndPreds = test.map { point =>
				  val prediction = model.predict(point.features)
				  (point.label, prediction)
				}
				val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
				
				
				
				val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
				val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
				file.append((auROC,MSE,accuracy,time).toString + "\tlambda: "+ l+ "\n")
				result=(auROC,MSE,accuracy)
			}
	    	return result
 		}
 	
 	def calculateLinearRegression(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) : (Double, Double, Double, Double, Double, Double) =
	 		{
 	  
 			val data=parsedData			
			// Split data into training (60%) and test (40%).
			val splits = data.randomSplit(Array(0.6, 0.4), seed = 11L)
			
			val training = splits(0).cache()
			val test = splits(1)
			
 			// Building the model
 			val file: Seekable =  Resource.fromFile(new File("output.txt"))
			var result:(Double, Double, Double, Double, Double, Double)= (0.0,0.0,0.0,0.0,0.0,0.0) 
			file.append("LR results\nROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t-\ttime\t-\ttimeL1\n")
				
 			
			val numIterations = List(50,100,200,500)
			for (nI<-numIterations){
				
				val LRalg = new LinearRegressionWithSGD()
				val s = System.currentTimeMillis
				val model = LRalg.run(training)
				val time= System.currentTimeMillis - s
				
				LRalg.optimizer.setUpdater(new L1Updater)
				 
				val s1 = System.currentTimeMillis
				val modelL1 = LRalg.run(training)  
				val time1= System.currentTimeMillis - s1
				
				
				/*********START Quality evaluation*******/
				
				// Compute raw scores on the test set. 
				val scoreAndLabels = test.map { point =>
				val score = model.predict(point.features)
				(score, point.label)
				}
			
				// Get evaluation metrics.
				val metrics = new BinaryClassificationMetrics(scoreAndLabels)
				val auROC = metrics.areaUnderROC()
				
				val scoreAndLabelsL1 = test.map { point =>
				val score = modelL1.predict(point.features)
				(score, point.label)
				}
			
				
				val metricsL1 = new BinaryClassificationMetrics(scoreAndLabelsL1)
				val auROCL1 = metricsL1.areaUnderROC()
				
				
				val valuesAndPreds = test.map { point =>
				  val prediction = model.predict(point.features)
				  (point.label, prediction)
				}
				val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
				
				
				val valuesAndPredsL1 = test.map { point =>
				  val prediction = modelL1.predict(point.features)
				  (point.label, prediction)
				}
				val MSEL1 = valuesAndPredsL1.map{case(v, p) => math.pow((v - p), 2)}.mean()
				
				val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
				val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
				val predictionAndLabelL1 = test.map(p => (modelL1.predict(p.features), p.label))
				val accuracyL1 = 1.0 * predictionAndLabelL1.filter(x => x._1 == x._2).count() / test.count()
				file.append((auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1,time,time1).toString + "\tn_it: "+ nI+ "\n")
				result=(auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1)
		    	
			}
 			return result
 			
 		}
 	
		def calculateSVM(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) :  (Double, Double, Double, Double, Double, Double) =
		{	//Load training data in LIBSVM format
			val data=parsedData			
			// Split data into training (60%) and test (40%).
			val splits = data.randomSplit(Array(0.6, 0.4), seed = 11L)
			
			val training = splits(0).cache()
			val test = splits(1)
			
			var numIterations = List(50,100,200,500)
			
			val file: Seekable =  Resource.fromFile(new File("output.txt"))
			var result:(Double, Double, Double, Double, Double, Double)= (0.0,0.0,0.0,0.0,0.0,0.0) 
			file.append("SVM results\nROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t-\ttime\t-\ttimeL1\n")
			
			for (nI<- numIterations){
			// Run training algorithm to build the model
			
			val svmAlg = new SVMWithSGD()
			svmAlg.optimizer.
			  setNumIterations(nI).
			  setRegParam(0.1)
			
			val s = System.currentTimeMillis
			val model = svmAlg.run(training)
			val time= System.currentTimeMillis - s
			
			svmAlg.optimizer.setUpdater(new L1Updater)
			 
			val s1 = System.currentTimeMillis
			val modelL1 = svmAlg.run(training)  
			val time1= System.currentTimeMillis - s1
			model.clearThreshold()
			modelL1.clearThreshold()
			
			
			/*********START Quality evaluation*******/
			
			// Compute raw scores on the test set. 
			val scoreAndLabels = test.map { point =>
			val score = model.predict(point.features)
			(score, point.label)
			}
		
			// Get evaluation metrics.
			val metrics = new BinaryClassificationMetrics(scoreAndLabels)
			val auROC = metrics.areaUnderROC()
			
			
			val scoreAndLabelsL1 = test.map { point =>
			val score = modelL1.predict(point.features)
			(score, point.label)
			}
		
			
			val metricsL1 = new BinaryClassificationMetrics(scoreAndLabelsL1)
			val auROCL1 = metricsL1.areaUnderROC()
			
			
			val valuesAndPreds = test.map { point =>
			  val prediction = model.predict(point.features)
			  (point.label, prediction)
			}
			val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			
			val valuesAndPredsL1 = test.map { point =>
			  val prediction = modelL1.predict(point.features)
			  (point.label, prediction)
			}
			val MSEL1 = valuesAndPredsL1.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
			val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
			val predictionAndLabelL1 = test.map(p => (modelL1.predict(p.features), p.label))
			val accuracyL1 = 1.0 * predictionAndLabelL1.filter(x => x._1 == x._2).count() / test.count()
			
			
			file.append((auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1,time,time1).toString + "\tn_it: "+ nI+ "\n")
			
	    	result=(auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1)

			}
			
			return result
			
	    }

 		
 		def createArray(values: List[String], table: Map[String, Int], count_table:Map[String, Int]) : Vector =
 		{				 
	    	var l : ListBuffer[(Int,Double)] = ListBuffer[(Int,Double)]() 
 		    table.foreach(x => {
 		    if (values.contains(x._1)) 
 		    	  l+= ((x._2,values.filter(t => t==x._1).size.toDouble))
 		    	})
 		    	
 		    if (l.length>0) 
	 		    {	val sv2: Vector = Vectors.sparse(table.size, l.sortBy(_._1))
	 		    	sv2
	 		    }
 		    else null
 		 }
 		
 		def createTagsTable(media : Double, count_table:Array[(String, Int)] ) : Map[String, Int] =
		{	/*create a table of first n tags count, or tags with count > avg*/
 		   // val valid_tags=count_table.filter(x=> x._2 > media).map(x => x._1).toSeq
 		    val valid_tags=count_table.sortBy(-_._2).map(x => x._1)
 		    val tagsIndexed=valid_tags.zipWithIndex
		    return tagsIndexed.toMap
 		}
 	
	 	def median(arr: Array[(String, Int)]) : Double = {
	 		val s= arr.map(x=>x._2).filter(x=> x>1)
	 		val (lower, upper) = s.sortWith(_<_).splitAt(s.size / 2)
	 		if (s.size % 2 == 0) (lower.last + upper.head).toDouble / 2 else upper.head
	 	}
	 	
	 	def obtainTDIDF (data: RDD[List[String]]) : RDD[Vector]=  
				      { 
					    val hashingTF = new HashingTF()
				      	val tf: RDD[Vector] = hashingTF.transform(data)
				      	tf.cache()
						val idf = new IDF().fit(tf)
						val tfidf: RDD[Vector] = idf.transform(tf)
						tfidf
				      }	
 		
	nu.pattern.OpenCV.loadShared()
	 	
 	val conf = new SparkConf().setMaster("local[4]").setAppName("FIB-Instagram")
	val sc = new SparkContext(conf)
	val sqlContext = new org.apache.spark.sql.SQLContext(sc)


	//for read from sequence file
	val rdd_im  = sc.sequenceFile[Text,BytesWritable]("data/wc2014/wc14_1.hsf")
	//val rdd_json  = sc.sequenceFile[Text,BytesWritable]("data/wc2014/wc14_1_meta.hsf")
	//val rdd_json  = sc.textFile("data/wc2014/worldcup2014_1/*.txt")
	val rdd_json  = sc.textFile("data/desigual-users-extended.txt")
	//val rdd_json  = sc.textFile("data/mydata.txt")
	val rdd_json2  = sc.textFile("data/mydata2.txt")
	//process the data in the RDD
	val process_im=rdd_im.map(x => (FilenameUtils.removeExtension(FilenameUtils.getBaseName(x._1.toString())),byteswritableToOpenCVMat(x._2).cols()+"")) //to do  with openCV
	//val process_json=rdd_json.map(x => (FilenameUtils.removeExtension(FilenameUtils.getBaseName(x._1.toString)),extractInfo(byteswritableToString(x._2))))
	//val process_json=rdd_json.map(x => extractInfo(x))
	val process_json=rdd_json.map(x => extractInfo(x)).filter(x => x != null)
	val process_json_neg=rdd_json2.map(x => extractInfo(x)).filter(x => x != null)
	
	//fill the table
	/* val path = "/home/alessio/Scrivania/sf_tesi/desigual-users-extended.txt"
	  
 	// Create a SchemaRDD from the file(s) pointed to by path
	val people = sqlContext.jsonFile(path)
	people.registerTempTable("people")
	val tags= sqlContext.sql("SELECT tags FROM people where tags is not null").map( x => x(0).asInstanceOf[scala.collection.mutable.ArrayBuffer[String]].mkString(" ").toLowerCase())
	val singleTags=tags.flatMap(line => line.split(" "))
    val counts = singleTags.map(tag => (tag, 1)).reduceByKey((a,b) => (a + b))
    */
    //create a table. Each line is represented by tag -> count of tag
    val tags=process_json.union(process_json_neg).map(x=>x.mkString(" ").toLowerCase())
    val singleTags=tags.flatMap(line => line.split(" "))
    val counts = singleTags.map(tag => (tag, 1)).reduceByKey((a,b) => (a + b))
    val tags_tmp=counts.toArray
	//val tags_tmp=counts.toArray
    //var tags_tmp = acc.value
    val avg : Double=counts.toArray.foldLeft(0)(_+_._2).toDouble/tags_tmp.length //calcuate the average of the mentioned tags
    val med = median(tags_tmp)
    //simple filter to remove unused tags
	var tags_table=createTagsTable(avg,tags_tmp)
	
    /*create RDD of Array[Double]. Each position of an array is an user.
	The number in position x indicate if the tag x is present is used by the user*/
	val data_tmp=process_json.map(x=> createArray(x,tags_table,tags_tmp.toMap)).filter(x=> x != null) //x._2 in caso di altro file
	val data_tmp_neg = process_json_neg.map(x=> createArray(x,tags_table,tags_tmp.toMap)).filter(x=> x != null) //x._2 in caso di altro file
	//val data_tmp_neg : RDD[Vector]= obtainTDIDF(process_json_neg)	
	
	val parsedData = data_tmp.map { line => LabeledPoint(1.0,line) }
	val parsedData_neg = data_tmp_neg.map { line => LabeledPoint(0.0,line) }
	
	println("END creation table\nbegin computation")
	val n_data=parsedData.count
	val n_data_neg=parsedData_neg.count
	val file: Seekable =  Resource.fromFile(new File("output.txt"))
	file.write("start\n")
	val parts= List(0.2,0.4,0.6,0.8,1.0)
	for (k<- parts)	{
		file.append("part of data: "+k+"\n")
		val dataF=sc.parallelize(parsedData.take((n_data*k).toInt)).union(sc.parallelize(parsedData_neg.take((n_data_neg*k).toInt)))
		
		
		var results : Array[Any] = new Array[Any](2)
		results(1)=calculateSVM(sc,dataF)
		results(0)=("ROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t")
		
		
		var results1 : Array[Any] = new Array[Any](2)
		results1(1)=calculateNaiveBayes(sc,dataF)
		results1(0)=("ROC\t-\tMSE\t-\taccurancy\t")
		
		
		var results2 : Array[Any] = new Array[Any](2)
		results2(1)=calculateLinearRegression(sc,dataF)
		results2(0)=("ROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t")
		
		println("\n\n\n\nResults of SVM:\n"+results(0))
		println(results(1))
		
	
		println("\n\n\n\nResults of Naive Bayes:\n"+results1(0))
		println(results1(1))
		
		
		println("\n\n\n\nResults of Linear Regression:\n"+results2(0))
		println(results2(1))
	}
  }
}