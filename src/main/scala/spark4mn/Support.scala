	package spark4mn
	
	import org.apache.spark.mllib.classification.NaiveBayes
	import org.apache.spark.SparkConf
	import org.apache.spark.SparkContext
	import org.apache.spark.SparkContext._
	import org.apache.spark.mllib.classification.SVMWithSGD
	import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
	import org.apache.spark.mllib.regression.LabeledPoint
	import org.apache.spark.mllib.linalg.Vectors
	import org.apache.hadoop.io.Text
	import org.apache.hadoop.io.BytesWritable
	import org.apache.spark.rdd.RDD
	import org.json4s._
	import org.json4s.native.JsonMethods._
	import org.opencv.core.Mat
	import org.opencv.highgui._
	import org.apache.commons.io.FilenameUtils
	import scala.collection.JavaConverters._
	import org.opencv.core.CvType
	import org.apache.spark.mllib.linalg.{Vector, Vectors}
	import org.apache.spark.mllib.regression.LinearRegressionWithSGD
	import org.apache.spark.mllib.feature.HashingTF
	import org.apache.spark.mllib.feature.IDF
	import org.apache.spark.mllib.optimization.L1Updater
	import scala.Array.canBuildFrom
	import org.apache.log4j.Logger
	import org.apache.log4j.Level

object Support {
 	
    def main(args: Array[String]) {
    
    	
      var path : String= 	{
 		if (args.length>=1)
 		   args(0)
 		 else if (args.length==2)
 		    args(1)
 		 else
 		   "./"
	}  
    
    if (path.last!='/') 
    	path+="/"
    
 		def featurize(s : String) : Vector =
 		{				 
	    	val n = 1000
	    	val result = new Array[Double](n)
	    	val bigrams= s.sliding(2).toArray
	    	for (h <- bigrams.map(_.hashCode() %n))
	    		{
	    			result(h) += 1.0 /bigrams.length
	    		}
	    	Vectors.sparse(n, result.zipWithIndex.filter(_._1 != 0).map(_.swap))
 		    
 		    
 		 }
 	def obtainTDIDF (data: RDD[List[String]]) : RDD[Vector]=  
				      { 
					    val hashingTF = new HashingTF()
				      	val tf: RDD[Vector] = hashingTF.transform(data)
				      	tf.cache()
						val idf = new IDF().fit(tf)
						val tfidf: RDD[Vector] = idf.transform(tf)
						tfidf
				      }	
 	def obtainFeaturize (data: RDD[List[String]]) : RDD[Vector]=  
					{
				      val tags2=data.map(x=>x.mkString(" "))
					    tags2.map(featurize)
					    }
 	
 	def calculate(sc: org.apache.spark.SparkContext){
 	 
 	  
	 	val sqlContext = new org.apache.spark.sql.SQLContext(sc)
	 	println("B")
	 	
		//process the data in the RDD
		
	    val name_file_pos="data/desigual-users-extended.txt" //or "data/mydata.txt" or "data/wc2014/worldcup2014_1/*.txt"
	    val name_file_neg="data/final_data.txt"
		
		val rdd_json  = sc.textFile(path+name_file_pos)
		val rdd_json2  = sc.textFile(path+name_file_neg)
	
		println("C")
	 	//Create a SchemaRDD from the file(s) pointed to by path for my algorithm
		val people = sqlContext.jsonFile(path+name_file_pos)
		people.registerTempTable("people")
		println("C1")
		val query_pos= sqlContext.sql("SELECT tags FROM people where tags is not null").map( x => x(0).asInstanceOf[scala.collection.mutable.ArrayBuffer[String]])
		val tags_pos=query_pos.map(x => x.mkString(" ").toLowerCase())
		val people_neg = sqlContext.jsonFile(path+name_file_neg)
		people_neg.registerTempTable("people_neg")
		println("C2")
		val query_neg=sqlContext.sql("SELECT tags FROM people_neg where tags is not null").map( x => x(0).asInstanceOf[scala.collection.mutable.ArrayBuffer[String]])
		val tags_neg= query_neg.map(x=> x.mkString(" ").toLowerCase())
		val tags2=tags_neg.union(tags_pos)
		println("C3")
		for (i <- 1 to 100)
			{val tags3=tags2.union(tags2).union(tags2)
			println("C4")
			val tags=tags3.union(tags3)
			println("C5")
			val singleTags=tags.flatMap(line => line.split(" "))
			println("C6 ")
		    val counts = singleTags.map(tag => (tag, 1))//.reduceByKey((a,b) => (a + b))
		    //create RDD for TDIDF and Featurize
		    val process_json=query_pos.map(x => x.toList)
			val process_json_neg=query_neg.map(x => x.toList)
			println("C7")
			//create table of (tag,count_tag)
			//val tags_tmp=counts.toArray
			println("C8")
			val avg : Double=counts.toArray.foldLeft(0)(_+_._2).toDouble //calcuate the average of the mentioned tags
		    println("D")
		    //simple filter to remove unused tags and creation of table (tag,index_tag)
			val c1=counts.map(x=> (x._1, x._2+avg.toInt ))
			val c2=c1.map(x=> (x._1.concat("_prova"),x._2.toDouble))
			val c3=c1.map(x=> (x._1,x._2.toDouble))
			val c4=c3.union(c2)
		    val c5=c4.filter(x=> x._2>=avg).filter(y=> y._1.contains("_prova"))
		    c5.first
		    val x=c3.map(x=> x._1).collect.toList
		    val (f,f1)=x.splitAt((x.length/2).toInt)
		    val r1=sc.parallelize(f)
		    val r2=sc.parallelize(f1)
		    val rf=r1.union(r2).glom.map(x=>x.toList)
		    rf.first
		    val op1=c4.map(x => (x._1,x._2*2))
		    val op2=c4.map(x => (x._1,x._2+2))
		    val op3=c4.map(x => (x._1,x._2/2))
		    val op4=c4.map(x => (x._1,x._2-2))
		    op1.first
		    op2.first
		    op3.first
		    op4.first
		    val o=obtainFeaturize(rf)
		    o.first
	        println("E")
		}
 	  
 	}
 	//Logger.getLogger("org").setLevel(Level.OFF)
 	//Logger.getLogger("akka").setLevel(Level.OFF)

 	println("INIZIO")
	//nu.pattern.OpenCV.loadShared()
 	val conf = new SparkConf().setAppName("FIB-Instagram").setMaster("local[4]").set("spark.executor.memory","4g").set("spark.driver.memory","4g").set("spark.driver.maxResultSize","3g")
 	
	val sc = new SparkContext(conf)
 	
 	println("A")
 	val a=System.currentTimeMillis()
	calculate(sc)
	val r=(System.currentTimeMillis())/1000
    println("FINE sec: "+r)
  }
}