	package spark4mn
	
	import org.apache.spark.mllib.classification.NaiveBayes
	import org.apache.spark.SparkConf
	import org.apache.spark.SparkContext
	import org.apache.spark.SparkContext._
	import org.apache.spark.mllib.classification.SVMWithSGD
	import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
	import org.apache.spark.mllib.evaluation.MulticlassMetrics
	import org.apache.spark.mllib.regression.LabeledPoint
	import org.apache.spark.mllib.linalg.Vectors
	import org.apache.hadoop.io.Text
	import org.apache.hadoop.io.BytesWritable
	import org.apache.spark.rdd.RDD
	import org.json4s._
	import org.json4s.native.JsonMethods._
	import org.opencv.core.Mat
	import org.opencv.highgui._
	import org.apache.commons.io.FilenameUtils
	import scala.collection.JavaConverters._
	import org.opencv.core.CvType
	import org.apache.spark.mllib.linalg.{Vector, Vectors}
	import scala.collection.mutable.ListBuffer
	import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
	import org.apache.spark.mllib.feature.HashingTF
	import org.apache.spark.mllib.feature.IDF
	import org.apache.spark.mllib.optimization.L1Updater
	import scalax.io._
	import java.io.File
	import java.text.SimpleDateFormat
	import java.util.Calendar
	
	
	
	
object Spark {
 	
    def main(args: Array[String]) {
      
    var path : String= 	{
 		if (args.length>=1)
 		   args(0)
 		 else if (args.length==2)
 		    args(1)
 		 else
 		   "./"
	}  
    
    if (path.last!='/') 
    	path+="/"
    
 
 	def byteswritableToOpenCVMat(inputBW: BytesWritable): Mat =
 	  {			val imageFileBytes = inputBW.getBytes
				var img = new Mat(1,imageFileBytes.length,CvType.CV_8UC1)
 	  			
 	  			img.put(0, 0, imageFileBytes)
 	  			img = Highgui.imdecode(img, Highgui.CV_LOAD_IMAGE_COLOR)
 	  		
			return img
 		 }
 	
 	def byteswritableToString(input : BytesWritable) : String =
 		{
 		  val conver = input.getBytes()
 		  val converted : String = new String(conver,0,conver.length,java.nio.charset.Charset.forName("Cp1252"))
 		  return converted
		
 		}
 	
 	
 	def extractInfo(input : String) : List[String] =
		{	implicit val formats = DefaultFormats
 		    
 		  	try{
				val s:String=input.stripLineEnd
 		  	    val conver = parse(s)
	 		  	val obj = conver \ "tags"
	 		  	
				val values : List[String]  = obj.extract[List[String]]
		        return values.map(x=>x.toLowerCase())
 		  	}
			catch{
			  case e: Exception => println(e+"\nerror json:\n" + input)
			  }
 			 
 			 return null
	    	}
 	   
 		
 		
 	 def calculateNaiveBayes(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) : (Double, Double, Double) =
	 		{
 	 	  
 	 	  	val file: Seekable =  Resource.fromFile(new File(path+"data/outputNB"))
			var result:( Double, Double, Double)= (0.0,0.0,0.0) 
			file.append("n_data: "+parsedData.count+"\n")
			file.append("Naive Bayes results\nROC\t-\tMSE\t-\taccurancy\t-\ttime\n")
			
 			val splits = parsedData.randomSplit(Array(0.6, 0.4), seed = 11L)
			val training = splits(0).cache()
			val test = splits(1)
			val lambdas= List(0.5,1.0,1.5)
			for (l<-lambdas){
				
				val s = System.currentTimeMillis
				val model = NaiveBayes.train(training, lambda = l)
				val time= System.currentTimeMillis - s
				
				/*********START Quality evaluation*******/
				
				// Compute raw scores on the test set. 
				val scoreAndLabels = test.map { point =>
				val score = model.predict(point.features)
				(score, point.label)
				}
			
				// Get evaluation metrics.
				val metrics = new BinaryClassificationMetrics(scoreAndLabels)
				val auROC = metrics.areaUnderROC()
				
				val valuesAndPreds = test.map { point =>
				  val prediction = model.predict(point.features)
				  (point.label, prediction)
				}
				val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
				
				
				
				val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
				val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
				
				val m=new MulticlassMetrics(scoreAndLabels)
				file.append(m.confusionMatrix.toString+"\n")
				
				
				file.append((auROC,MSE,accuracy,time).toString + "\tlambda: "+ l+ "\n")
				result=(auROC,MSE,accuracy)
			}
	    	return result
 		}
 	
 	 def calculateLogisticRegression(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) : (Double, Double, Double, Double, Double, Double) =
	 		{
 	  
 			val data=parsedData			
			// Split data into training (60%) and test (40%).
			val splits = data.randomSplit(Array(0.6, 0.4), seed = 11L)
			
			val training = splits(0).cache()
			val test = splits(1)
			
 			// Building the model
 			val file: Seekable =  Resource.fromFile(new File(path+"data/outputLR"))
			var result:(Double, Double, Double, Double, Double, Double)= (0.0,0.0,0.0,0.0,0.0,0.0) 
			file.append("n_data: "+parsedData.count+"\n")
			file.append("LR results\nROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t-\ttime\t-\ttimeL1\n")
				
 			
			val numIterations = List(50,100,200,500)
			
			for (nI<-numIterations){
				
				val LRalg = new  LogisticRegressionWithSGD().setValidateData(true)
				LRalg.optimizer.setNumIterations(nI)
				val s = System.currentTimeMillis
				val model = LRalg.run(training)
				val time= System.currentTimeMillis - s
				
				LRalg.optimizer.setUpdater(new L1Updater)
				 
				val s1 = System.currentTimeMillis
				val modelL1 = LRalg.run(training)  
				val time1= System.currentTimeMillis - s1
				
				
				/*********START Quality evaluation*******/
				
				// Compute raw scores on the test set. 
				val scoreAndLabels = test.map { point =>
				val score = model.predict(point.features)
				(score, point.label)
				}
			
				// Get evaluation metrics.
				val metrics = new BinaryClassificationMetrics(scoreAndLabels)
				val auROC = metrics.areaUnderROC()
				
				val scoreAndLabelsL1 = test.map { point =>
				val score = modelL1.predict(point.features)
				(score, point.label)
				}
			
				
				val metricsL1 = new BinaryClassificationMetrics(scoreAndLabelsL1)
				val auROCL1 = metricsL1.areaUnderROC()
				
				
				val valuesAndPreds = test.map { point =>
				  val prediction = model.predict(point.features)
				  (point.label, prediction)
				}
				val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
				
				
				val valuesAndPredsL1 = test.map { point =>
				  val prediction = modelL1.predict(point.features)
				  (point.label, prediction)
				}
				val MSEL1 = valuesAndPredsL1.map{case(v, p) => math.pow((v - p), 2)}.mean()
				
				val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
				val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
				val predictionAndLabelL1 = test.map(p => (modelL1.predict(p.features), p.label))
				val accuracyL1 = 1.0 * predictionAndLabelL1.filter(x => x._1 == x._2).count() / test.count()
				
				val mL2=new MulticlassMetrics(scoreAndLabels)
				file.append(mL2.confusionMatrix.toString+"\n")
				val mL1=new MulticlassMetrics(scoreAndLabelsL1)
				file.append(mL1.confusionMatrix.toString+"\n")
				
				file.append((auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1,time,time1).toString + "\tn_it: "+ nI+ "\n")
				result=(auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1)
		    	
			}
 			return result
 			
 		}
 	
	def calculateSVM(sc: SparkContext ,  parsedData: RDD[LabeledPoint]) :  (Double, Double, Double, Double, Double, Double) =
		{	//Load training data in LIBSVM format
			val data=parsedData			
			// Split data into training (60%) and test (40%).
			val splits = data.randomSplit(Array(0.6, 0.4), seed = 10L)
			
			val training = splits(0).cache()
			val test = data
			
			var numIterations = List(50,100,200,500)
			
			val file: Seekable =  Resource.fromFile(new File(path+"data/outputSVM"))
			var result:(Double, Double, Double, Double, Double, Double)= (0.0,0.0,0.0,0.0,0.0,0.0) 
			file.append("n_data: "+parsedData.count+"\n")
			file.append("SVM results\nROC L2 - ROC L1\t-\tMSE L2\t-\tMSE L1\t-\taccurancy L2\t-\taccurancy L1\t-\ttime\t-\ttimeL1\n")
			
			for (nI<- numIterations){
			// Run training algorithm to build the model
			
			  
			val svmAlg = new SVMWithSGD().setValidateData(true)
			svmAlg.optimizer.
			  setNumIterations(nI)
			  
			
			val s = System.currentTimeMillis
			val model = svmAlg.run(training)
			val time= System.currentTimeMillis - s
			
			svmAlg.optimizer.setUpdater(new L1Updater)
			 
			val s1 = System.currentTimeMillis
			val modelL1 = svmAlg.run(training)  
			val time1= System.currentTimeMillis - s1
			model.clearThreshold()
			modelL1.clearThreshold()
			
			
			/*********START Quality evaluation*******/
			
			// Compute raw scores on the test set. 
			val scoreAndLabels = test.map { point =>
			val score = model.predict(point.features)
			(score, point.label)
			}
				
			val scoreAndLabelsL1 = test.map { point =>
			val score = modelL1.predict(point.features)
			(score, point.label)
			}
			
			// Get evaluation metrics.
			val metrics = new BinaryClassificationMetrics(scoreAndLabels)
			val auROC = metrics.areaUnderROC()
			
		
			val metricsL1 = new BinaryClassificationMetrics(scoreAndLabelsL1)
			val auROCL1 = metricsL1.areaUnderROC()
			
			
			val valuesAndPreds = test.map { point =>
			  val prediction = model.predict(point.features)
			  (point.label, prediction)
			}
			
			val valuesAndPredsL1 = test.map { point =>
			  val prediction = modelL1.predict(point.features)
			  (point.label, prediction)
			}
			
			val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
			val MSEL1 = valuesAndPredsL1.map{case(v, p) => math.pow((v - p), 2)}.mean()
			
			val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
			val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()
			val predictionAndLabelL1 = test.map(p => (modelL1.predict(p.features), p.label))
			val accuracyL1 = 1.0 * predictionAndLabelL1.filter(x => x._1 == x._2).count() / test.count()
			
			val mL2=new MulticlassMetrics(scoreAndLabels)
			file.append(mL2.confusionMatrix.toString+"\n")
			val mL1=new MulticlassMetrics(scoreAndLabelsL1)
			file.append(mL1.confusionMatrix.toString+"\n")
			
			file.append((auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1,time,time1).toString + "\tn_it: "+ nI+ "\n")
			
	    	result=(auROC,auROCL1,MSE,MSEL1,accuracy,accuracyL1)

			}
			
			return result
			
	    }

 		
 	def createArray(values: List[String], table: Map[String, Int], count_table:Map[String, Int]) : Vector =
 		{				 
	    	var l : ListBuffer[(Int,Double)] = ListBuffer[(Int,Double)]() 
 		    table.foreach(x => {
 		    if (values.contains(x._1)) 
 		    	  l+= ((x._2,values.filter(t => t==x._1).size.toDouble))
 		    	})
 		    	
 		    if (l.length>0) 
	 		    {	val sv2: Vector = Vectors.sparse(table.size, l.sortBy(_._1))
	 		    	sv2
	 		    }
 		    else null
 		 }
 		
 	def createTagsTable(media : Double, count_table:Array[(String, Int)] ) : Map[String, Int] =
		{	/*create a table of first n tags count, or tags with count > avg*/
 		   // val valid_tags=count_table.filter(x=> x._2 > media).map(x => x._1).toSeq
 		    val valid_tags=count_table.sortBy(-_._2).map(x => x._1)
 		    val tagsIndexed=valid_tags.zipWithIndex
		    return tagsIndexed.toMap
 		}
 	
	def median(arr: Array[(String, Int)]) : Double = {
	 		val s= arr.map(x=>x._2).filter(x=> x>1)
	 		val (lower, upper) = s.sortWith(_<_).splitAt(s.size / 2)
	 		if (s.size % 2 == 0) (lower.last + upper.head).toDouble / 2 else upper.head
	 	}
	 	
	def featurize(s : String) : Vector =
 		{				 
	    	val n = 1000
	    	val result = new Array[Double](n)
	    	val bigrams= s.sliding(2).toArray
	    	for (h <- bigrams.map(_.hashCode() %n))
	    		{
	    			result(h) += 1.0 /bigrams.length
	    		}
	    	Vectors.sparse(n, result.zipWithIndex.filter(_._1 != 0).map(_.swap))
 		    
 		    
 		 }
 	
	def obtainTDIDF (data: RDD[List[String]]) : RDD[Vector]=  
				      { 
					    val hashingTF = new HashingTF()
				      	val tf: RDD[Vector] = hashingTF.transform(data)
				      	tf.cache()
						val idf = new IDF().fit(tf)
						val tfidf: RDD[Vector] = idf.transform(tf)
						tfidf
				      }	
 	
    def obtainFeaturize (data: RDD[List[String]]) : RDD[Vector]=  
					{
				      val tags2=data.map(x=>x.mkString(" "))
					    tags2.map(featurize)
					    }
	//nu.pattern.OpenCV.loadShared()
	
 	val conf = new SparkConf().setMaster("local[4]").setAppName("FIB-Spark")
	val sc = new SparkContext(conf)
	val sqlContext = new org.apache.spark.sql.SQLContext(sc)

    val name_file_pos="data/desigual-users-extended.txt" //or "data/mydata.txt" or "data/wc2014/worldcup2014_1/*.txt"
    val name_file_neg="data/final_data.txt"
	//for read from sequence file
	val rdd_im  = sc.sequenceFile[Text,BytesWritable](path+"data/wc2014/wc14_1.hsf")
	//val rdd_json  = sc.sequenceFile[Text,BytesWritable](path+"data/wc2014/wc14_1_meta.hsf")
	
	val rdd_json  = sc.textFile(path+name_file_pos)
	val rdd_json2  = sc.textFile(path+name_file_neg)
	//process the data in the RDD
	/*
	//val process_im=rdd_im.map(x => (FilenameUtils.removeExtension(FilenameUtils.getBaseName(x._1.toString())),byteswritableToOpenCVMat(x._2).cols()+"")) //to do  with openCV
	//val process_json=rdd_json.map(x => (FilenameUtils.removedExtension(FilenameUtils.getBaseName(x._1.toString)),extractInfo(byteswritableToString(x._2))))
	val process_json=rdd_json.map(x => extractInfo(x)).filter(x => x != null)
	val process_json_neg=rdd_json2.map(x => extractInfo(x)).filter(x => x != null)
	
	//fill the table
	 
    //create a table. Each line is represented by tag -> count of tag
    val tags=process_json.union(process_json_neg).map(x=>x.mkString(" ").toLowerCase())
    val singleTags=tags.flatMap(line => line.split(" "))
    val counts = singleTags.map(tag => (tag, 1)).reduceByKey((a,b) => (a + b))
    */
	
 	//Create a SchemaRDD from the file(s) pointed to by path for my algorithm
	val people = sqlContext.jsonFile(path+name_file_pos)
	people.registerTempTable("people")
	val query_pos= sqlContext.sql("SELECT tags FROM people where tags is not null").map( x => x(0).asInstanceOf[scala.collection.mutable.ArrayBuffer[String]])
	val tags_pos=query_pos.map(x => x.mkString(" ").toLowerCase())
	val people_neg = sqlContext.jsonFile(path+name_file_neg)
	people_neg.registerTempTable("people_neg")
	val query_neg=sqlContext.sql("SELECT tags FROM people_neg where tags is not null").map( x => x(0).asInstanceOf[scala.collection.mutable.ArrayBuffer[String]])
	val tags_neg= query_neg.map(x=> x.mkString(" ").toLowerCase())
	val tags=tags_neg.union(tags_pos)
	val singleTags=tags.flatMap(line => line.split(" "))
    val counts = singleTags.map(tag => (tag, 1)).reduceByKey((a,b) => (a + b))
    //create RDD for TDIDF and Featurize
    val process_json=query_pos.map(x => x.toList)
	val process_json_neg=query_neg.map(x => x.toList)
	val a = query_neg.count
	val b=  query_pos.count
	//create table of (tag,count_tag)
	val tags_tmp=counts.toArray
	val avg : Double=counts.toArray.foldLeft(0)(_+_._2).toDouble/tags_tmp.length //calcuate the average of the mentioned tags
    val med = median(tags_tmp)
    //simple filter to remove unused tags and creation of table (tag,index_tag)
	var tags_table=createTagsTable(avg,tags_tmp)
	
    
	/*create RDD of Array[Double]. Each position of an array is an user.
	The number in position x indicate if the tag x is present is used by the user*/
	
	val data_tmp=process_json.map(x=> createArray(x,tags_table,tags_tmp.toMap)).filter(x=> x != null) //x._2 in caso di altro file
	val data_tmp_neg = process_json_neg.map(x=> createArray(x,tags_table,tags_tmp.toMap)).filter(x=> x != null) //x._2 in caso di altro file
		
	
	val parsedData = data_tmp.map { line => LabeledPoint(1.0,line) }
	val parsedData_neg = data_tmp_neg.map { line => LabeledPoint(0.0,line) }
	//create 4 other dataSet(2 pos - 2 neg) , 2 with featurize function, other 2 with TDIDF function
	val data_tmpFeaturize : RDD[Vector]= obtainFeaturize(process_json)
	val data_tmpTDIDF : RDD[Vector]= obtainTDIDF(process_json)
	val data_tmpFeaturize_neg : RDD[Vector]= obtainFeaturize(process_json_neg)
	val data_tmpTDIDF_neg : RDD[Vector]= obtainTDIDF(process_json_neg)
 					
	val parsedData_Fea = data_tmpFeaturize.map { line => LabeledPoint(1.0,line) }
	val parsedData_FeaNeg = data_tmpFeaturize_neg.map { line => LabeledPoint(0.0,line) }
	val parsedData_TDIDF = data_tmpTDIDF.map { line => LabeledPoint(1.0,line) }
	val parsedData_TDIDFNeg = data_tmpTDIDF_neg.map { line => LabeledPoint(0.0,line) }
	
	
	//val finalDataFea=parsedData_Fea.union(parsedData_FeaNeg)
	//val finalDataTDIDF=parsedData_TDIDF.union(parsedData_TDIDFNeg)
	
	
	val parts= List(1.0)
	val l_data_pos= List(parsedData,parsedData_Fea,parsedData_TDIDF)
	val l_data_neg= List(parsedData_neg,parsedData_FeaNeg,parsedData_TDIDFNeg)
	val format = new SimpleDateFormat("yyyy-M-dd")
	
	val fileSVM: Seekable =  Resource.fromFile(new File(path+"data/outputSVM")) 
    val fileLR: Seekable =  Resource.fromFile(new File(path+"data/outputLR"))
	val fileNB: Seekable =  Resource.fromFile(new File(path+"data/outputNB"))
	fileSVM.append(format.format(Calendar.getInstance().getTime())+"\n")
    fileLR.append(format.format(Calendar.getInstance().getTime())+"\n")
    fileNB.append(format.format(Calendar.getInstance().getTime())+"\n")
    
	for (i<- 1 until 3){
		
	   if (i==0) {
	      fileSVM.append("MY_FEATURIZE\n")
	      fileLR.append("MY_FEATURIZE\n")
	      fileNB.append("MY_FEATURIZE\n")
	    }
	    else if (i==1)
	      {fileSVM.append("ORIGINAL_FEATURIZE\n")
	      fileLR.append("ORIGINAL_FEATURIZE\n")
	      fileNB.append("ORIGINAL_FEATURIZE\n")
	    }
	    else if (i==2)  {fileSVM.append("TDIDF\n")
	      fileLR.append("TDIDF\n")
	      fileNB.append("TDIDF\n")
	    }
	    
	    val n_data=l_data_pos(i).count
		val n_data_neg=l_data_neg(i).count
		
		for (k<- parts)	{
			
			val dataF=sc.parallelize(l_data_pos(i).take((n_data*k).toInt)).union(sc.parallelize(l_data_neg(i).take((n_data_neg*k).toInt)))
			//val dataF=sc.parallelize(parsedData.take(10)).union(sc.parallelize(parsedData_neg.take(100)))
			
			calculateSVM(sc,dataF)
			calculateNaiveBayes(sc,dataF)
			calculateLogisticRegression(sc,dataF)
		}
	}
 }
}
