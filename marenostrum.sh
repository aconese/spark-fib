### COMPULSORY PARAMETERS:
  JOB_NAME="shuffle_1t_10000p_256w_4cpw"
  WORKING_DIR="/home/bsc31/bsc31886/test_batchs"
  WALLCLOCK=60
  NWORKERS=256
  NWORKERS_PER_NODE=2
  EXECUTOR_JAR_1="/home/bsc31/bsc31886/bsc.spark.test/bsc.spark-1.0.jar"
  EXECUTOR_ENTRY_POINT_1="bsc.spark.examples.terasort.rxin.TeraGenAndShuffle"

### OPTIONAL PARAMETERS:
    SPARK_VERSION="1.1.1" # Must match with the proper Spark module version.
  # ETH_NETWORK="[BOOL]" # If set, and true, communication will go through the ethernet network. Otherwise it will go over Infiniband.
  # QUEUE="[STRING]" # If it is set, the job will go to this specific queue.
  # X11="[BOOL]" # If the job has graphics is needed, otherwise it will fail.
  # ADDITIONAL_MODULES="[[NAME | NAME/VERSION], [...]]" # Just in case you need additional modules.
  # ADDITIONAL_CLASSPATH="[PATH:PATH]" # Just in case you need additional classes in the classpath.
  # NCORES_PER_WORKER=[INTEGER] # If not set, it will be floor(NCORES/NWORKERS_PER_NODE) (with a minimum of 1 core).
  # WORKER_MEM_SIZE=[INTEGER] # Megabytes. # If not set, it will be floor(NODE_MEM_SIZE/NWORKERS_PER_NODE) (with a minimum of 1 MB).
  # WORKER_AFFINITY="[CORE | SOCKET | NODE | NONE]" # If not set, it will wall into NONE mode.
  # DATA_LOAD_JAR_[[1-9]+]="[PATH]"
  # DATA_LOAD_ENTRY_POINT_[[1-9]+]="[STRING]" 
  # DATA_LOAD_INPUT_PATH_[[1-9]+]="[PATH]"
  # DATA_LOAD_ADDITIONAL_PARAMETERS_[[1-9]+]="[STRING]" # All paths here must be absolute.
  # EXECUTOR_OUTPUT_PATH_[[1-9]+]="[PATH]"
  EXECUTOR_ADDITIONAL_PARAMETERS_1="1t 10000 10000" # All paths here must be absolute

